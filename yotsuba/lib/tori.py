# -*- coding: utf-8 -*-
'''
**Tori web framework**  is a simple web framework working on the top of CherryPy
3.1+. It is designed to get started with CherryPy without repeating the commonly
used configuration or procedure to run the application.

It is also designed to with deployment in mind where the app starter can be used
as standalone WSGI server, WSGI application (wrapper) or Google App Engine application.
'''

__version__ = 1.1
__scan_only__ = [
    ('response', 'multiple-key dictionary', 'See cherrypy.response for more detail.'),
    ('request', 'multiple-key dictionary', 'See cherrypy.request for more detail.'),
    ('session', 'dict', 'See cherrypy.session for more detail.'),
    'ServiceMode',
    'ServerInterface',
    'setup',
    'start_session',
    'render',
    'minify_content',
    'WebFrameworkException',
    'BaseInterface',
    'RESTInterface'
]

import os
import re
import sys
from mimetypes import guess_type
from time import time
from wsgiref import handlers

import cherrypy

from yotsuba.core       import base, fs
from yotsuba.lib.kotoba import Kotoba

from mako               import __version__ as mako_version
from mako.template      import Template
from mako.lookup        import TemplateLookup
from mako.exceptions    import html_error_template

from google.appengine.api import memcache

# Global variables
debug = False
settings = {}
memory = {}
template = None

response = cherrypy.response
request = cherrypy.request
session = None

log = base.logger.getBasic("Yotsuba/Tori")

##################
# Setup function #
##################

class ServiceMode(object):
    '''
    ServiceMode is just a group of keywords to tell ServerInterface which mode to run.
    
    This class provides only static attributes. You may instantiate but nothing works.
    '''
    __scan_only__ = [
        ('server', 'str', 'Tell the service script to run the application in WSGI server mode (standalone).'),
        ('application', 'str', 'Tell the service script to run the application in WSGI application/wrapper mode with a web server like *Apache* 2.x with *mod_wsgi*).'),
        ('GAE', 'str', 'Tell the service script to run the application in WSGI application/wrapper mode on Google App Engine.')
    ]
    server = "local"
    application = "application"
    GAE = "gae"

class ServerInterface(object):
    '''
    ServerInterface is like a switch to control how you deploy your application.
    
    It supports three modes described in *tori.ServiceMode*.
    '''
    
    @staticmethod
    def application(*largs, **kwargs):
        '''
        Run the application in WSGI application/wrapper mode with a web server
        or on Google App Engine.
        
        To run the application on Google App Engine, it is not recommended to
        use this function directly. Please use *ServerInterface.auto* instead.
        '''
        global base_uri
        global static_routing
        
        largs = list(largs)
        
        if len(largs) < 2:
            largs.append(base_uri)
        
        cherrypy.config.update({
            'environment': 'embedded'
        })
        
        if 'config' in kwargs:
            static_routing = kwargs['config']
        
        application = cherrypy.Application(*largs, **kwargs)
        application.merge(static_routing)
        
        return application
    
    @staticmethod
    def auto(*largs, **kwargs):
        '''
        Run in auto mode. This is vary, depending on the configuration.
        '''
        application = ServerInterface.application(*largs, **kwargs)
        handlers.CGIHandler().run(application)
        
        return application

def setup(configuration_filename):
    """
    Set up the environment
    
    `configuration_filename` is the name of the configuration file in XML. By
    default, it is null and instruct the system to use the default settings.
    
    """
    
    global mode
    global base_uri
    global base_path
    global path
    global error_template
    global static_routing
    global settings
    global template
    global debug
    global TEMPLATE_DIRS # Only for Django
    
    # Initialization
    __base_config_key = 'tools.static'
    mode = ServiceMode.GAE
    port = 8080
    base_uri = '/'
    base_path = ''
    path = {}
    error_template = None
    static_routing = {}
    default_config = {
        'global': {
            'tools.decode.encoding':    'utf-8',
            'tools.encode.encoding':    'utf-8',
            'tools.decode.on':          True,
            'tools.encode.on':          True,
            'log.screen':               False # Disable trackback information
        }
    }
    
    settings = {}
    
    # Get the reference to the calling function
    f = sys._getframe(1)
    c = f.f_code
    reference_to_caller = c.co_filename
    
    # Base path
    base_path = os.path.abspath(os.path.dirname(os.path.abspath(reference_to_caller)))
    
    # Get the location of the given the configuration files
    target_destination = configuration_filename
    if not re.search('^/', configuration_filename):
        target_destination = "%s/%s" % (base_path, configuration_filename)
    
    # Load the configuration files
    xmldoc = Kotoba()
    xmldoc.read(target_destination)
    
    # Store the basic paths in the memory. Make the directory for the destination if necessary.
    path['template'] = os.path.join(base_path, 'template')
    
    # Get application settings
    xml_on_settings = xmldoc.get("option")
    for option in xml_on_settings:
        option_data = option.data()
        option_data_as_boolean = base.convertToBoolean(option_data)
                
        if option_data_as_boolean is not None:
            # the option data is boolean-convertible.
            option_data = option_data_as_boolean
                
        settings[option.attrs['name']] = option_data
        
    # Recognized options by framework (with the default value)
    recognized_options = {
        'no_cache': True
    }
        
    for recognized_option, default_option in recognized_options.iteritems():
        if recognized_option not in settings:
            settings[recognized_option] = default_option
    
    template = TemplateInterface(path['template'])
    
    cherrypy.config.update(default_config)

def start_session():
    '''
    Start session (CherryPy)
    '''
    try:
        global session
        if not session:
            session = cherrypy.session
        return True
    except:
        return False

######################
# Rendering function #
######################

def render(source, **kwargs):
    '''
    *Deprecated in Yotsuba 4*
    
    See *TemplateInterface.render*.
    '''
    global template
    return template.render(source, **kwargs)

#############
# Exception #
#############

class WebFrameworkException(Exception):
    '''Exception specifically used by Tori during initilization'''
    __scan_only__ = []

class CherryPyException(Exception):
    '''Exception when dealing with errors in CherryPy wrappers which may be caused by CherryPy itself'''

##############
# Decorators #
##############

webinterface = cherrypy.expose
webinterface.__doc__ = ''' Use this method as a web interface. (Based cherrypy.expose) '''

######################
# Template Interface #
######################

class TemplateInterface(object):
    '''
    Template Interface
    '''
    __default_options = {
        'input_encoding':       'utf-8',
        'output_encoding':      'utf-8',
        'encoding_errors':      'replace'
    }
    __cache = {}
    
    def __init__(self, *directories, **options):
        self.__directories   = directories
        
        template_params = {
            'directories': self.__directories
        }
        
        template_params.update(self.__default_options)
        
        template_params.update(options)
        
        self.__engine =  TemplateLookup(**template_params)
    
    def render(self, source, **kwargs):
        '''
        This function is to render Mako templates.
        
        This function *can* have more than *one* parameter. There are *three* optional
        parameters only used by this function which are described below. The *rest* are
        context variables for rendering.
        
        The required parameter *source* is a string indicating the location of the
        template. If the optional parameter *direct_rendering* is false, it will
        attempt to render the *source* directly as if it is a template data.
        
        The optional parameter *direct_rendering* is a boolean acting as a switch to
        enable or disable the behaviour described above. This option may be set
        globally in the configuration file. It is set to 'False' by default.
        
        The optional parameter *text_minification* is a boolean acting as a switch to
        enable or disable auto HTML minification. This option may be set globally in
        the configuration file. Currently, it doesn't fully support JavaScript
        minification. It is set to 'False' by default.
        
        Any parameters beside the three ones above will be treated as template context.
        '''
        global template
        
        # Render with Mako
        for directory in self.__directories:
            if os.path.exists(os.path.join(directory, source)):
                actual_source = os.path.join(directory, source)
                compiled_template = self.__engine.get_template(source)
                break
            
        try:
            return compiled_template.render_unicode(**kwargs)
        except:
            raise Exception(html_error_template().render())

####################
# Basic Interfaces #
####################

class BaseInterface(object):
    '''
    Base interface provides all basic functionalities for developing a web interface
    responsible for processing and responding incoming HTTP requests. Only compatible with CherryPy 3.1+.
    '''
    __class__ = "BaseInterface"
    
    def method(self):
        ''' Return the request method (GET, POST, DELETE, PUT) '''
        return cherrypy.request.method
    
    def isGET(self):
        ''' Check if this request is a GET request. '''
        return self.method() == "GET"
    
    def isPOST(self):
        ''' Check if this request is a POST request. '''
        return self.method() == "POST"
    
    def isDELETE(self):
        ''' Check if this request is a DELETE request. '''
        return self.method() == "DELETE"
    
    def isPUT(self):
        ''' Check if this request is a PUT request. '''
        return self.method() == "PUT"
    
    def isAuth(self):
        ''' Abstract: Check if the user is authenticated. '''
        return True
    
    def isAdmin(self):
        ''' Abstract: Check if the user is an administrator. '''
        return True
    
    def render(self, source, **kwargs):
        '''
        Wrapper method for *tori.BaseInterface.default_render*
        
        Example: self.render('shiroyuki.html', author='juti noppornpitak', direct_rendering=False)
        '''
        return self.default_render(source, **kwargs)
    
    def default_render(self, source, **kwargs):
        '''
        Render template from *source* with kwargs as context variables/options.
        
        For rendering options, please see *tori.render*.
        
        Example: self.default_render('shiroyuki.html', author='juti noppornpitak', direct_rendering=False)
        '''
        global template
        response.headers['Platform'] = base.getVersion() + " / Tori %s" % __version__
        kwargs['base_uri'] = base_uri
        return template.render(source, **kwargs)
    
    def respond_status(self, code, message = None, break_now = True):
        '''
        Set the response status.
        
        *code* is a HTTP status code in interger.
        
        *message* is a response reason.
        
        *break_now* is a boolean. If *True*, this method will raise CherryPy's
        HTTPError exception. Otherwise, return status.
        '''
        if not base.isString(message) or len(message) == 0:
            message = None
        
        if 300 <= code < 400:
            del cherrypy.response.headers['Content-Type']
        
        if code >= 400 and break_now:
            if message:
                raise cherrypy.HTTPError(code, message)
            else:
                raise cherrypy.HTTPError(code)
        else:
            if message:
                cherrypy.response.status = u"%d %s" % (code, message)
            else:
                cherrypy.response.status = code
        
        return base.convertToUnicode(cherrypy.response.status)
    
    def redirect(self, url, code=None, soft_redirection=True):
        '''
        Redirect a URL with CherryPy's HTTPRedirect (exception).
        
        *url* is a string representing URL.
        
        *code* is a HTTP status code in interger.
        
        *soft_redirection* make this method not to raise a redirection exception
        by CherryPy. This is more customizable to what to be given back. This
        option is strongly recommended for Google App Engine applications.
        '''
        if soft_redirection:
            response.status = code or 302
            response.headers['Location'] = url
        else:
            try:
                if code and base.isIntegerNumber(code):
                    # external redirect with code
                    raise cherrypy.HTTPRedirect(url, code)
                else:
                    # external redirect without code
                    raise cherrypy.HTTPRedirect(url)
            except:
                raise CherryPyException, "Cannot redirect with CherryPy here."

class RESTInterface(BaseInterface):
    '''
    Abstract class for a RESTful Interface based on *BaseInterface*. Only compatible with CherryPy 3.1+.
    '''
    __class__ = "RESTInterface"
    
    def __init__(self):
        self.throw_all_error = False

    # Medium-level functions that directly handle data by methods.
    def index(self):
        '''
        List resources

        GET ./
        '''
        self.respond_status(405)

    def new(self):
        '''
        Prompt to create a new resource

        GET ./new
        '''
        self.respond_status(405)

    def create(self):
        '''
        Create a new resource

        POST ./
        '''
        self.respond_status(405)

    def edit(self, key):
        '''
        Prompt to update a new resource

        GET ./{key}/edit
        '''
        self.respond_status(405)

    def update(self, key):
        '''
        Update the target resource

        POST ./{key}/update
        PUT  ./{key}
        '''
        self.respond_status(405)

    def show(self, key):
        '''
        Show the target resource

        GET ./{KEY}
        '''
        self.respond_status(405)

    def destroy(self, key):
        '''
        Delete the target resource

        POST ./{KEY}/delete
        DEL  ./{KEY}
        '''
        self.respond_status(405)

    # Medium-level functions that directly handle data by methods.
    def get(self, *args, **kwargs):
        '''
        Handle GET request
        
        GET ./*
        '''
        output = None
        if args and args[-1] == 'new':
            output = self.new(*args[:-1], **kwargs)
        elif args and args[-1] == 'edit':
            output = self.edit(*args[:-1], **kwargs)
        elif args:
            output = self.show(*args, **kwargs)
        else:
            output = self.index(*args, **kwargs)
        return output
    
    def post(self, *args, **kwargs):
        '''
        Handle POST request
        
        POST ./
        '''
        output = None
        if args and args[-1] == 'update':
            output = self.update(*args, **kwargs)
        elif args and args[-1] == 'delete':
            output = self.destroy(*args, **kwargs)
        else:
            output = self.create(*args, **kwargs)
        return output
    
    def put(self, *args, **kwargs):
        '''
        Handle PUT request
        
        PUT ./*
        '''
        return self.update(*args, **kwargs)
    
    def delete(self, *args, **kwargs):
        '''
        Handle DELETE request
        
        DELETE ./*
        '''
        return self.destroy(*args, **kwargs)

    def default(self, *args, **kwargs):
        '''
        Low-level function handling requests based on the method.
        '''
        result = None
        if self.throw_all_error:
            result = self.__default(*args, **kwargs)
        else:
            try:
                result = self.__default(*args, **kwargs)
            except TypeError:
                self.respond_status(400)
        return result
    default.exposed = True
    
    def __default(self, *args, **kwargs):
        '''
        Low-level function handling requests based on the method.
        '''
        result = None
        if self.isPOST():
            result = self.post(*args, **kwargs)
        elif self.isPUT():
            result = self.put(*args, **kwargs)
        elif self.isDELETE():
            result = self.delete(*args, **kwargs)
        else:
            result = self.get(*args, **kwargs)
        return result

#############
# Utilities #
#############

def make_directory_if_not_existed(destination):
    if not fs.exists(destination) and mode != ServiceMode.GAE:
        try:
            fs.mkdir(destination)
        except:
            raise WebFrameworkException("%s not found" % destination)
