# -*- encoding: utf-8 -*-
from os.path import join
from django.utils import simplejson
from yotsuba.core import net
from yotsuba.lib import tori
from yotsuba.lib.kotoba import Kotoba as xml
from vps.exception import NoTranslation

class UI(object):
    engine = None
    lang_english    = u"English"
    lang_thai       = u"ภาษาไทย"
    
    @staticmethod
    def label_for_lang_switch(lang):
        return lang == "th" and UI.lang_english or UI.lang_thai
    
    @staticmethod
    def init():
        UI.engine = xml(join(tori.base_path, 'translation.xml'))
    
    @staticmethod
    def has_translation(lang):
        UI.init()
        return UI.engine.get("translation[lang=%s]" % lang)
    
    @staticmethod
    def translate(lang, key, *args):
        UI.init()
        label = UI.engine.get("label[key=%s] %s" % (key, lang))
        if not label: raise NoTranslation, "No translation for '%s' in '%s'" % (key, lang)
        label = label.data() % args
        return label
    
    @staticmethod
    def button_add(href, text):
        return '<a href="%s" class="button add"><img src="/icon/black/add-item.png"/> %s</a>' % (href, text)