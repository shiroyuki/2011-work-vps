# -*- encoding: utf-8 -*-
from pickle import loads, dumps
from base64 import b64encode as en
from base64 import b64decode as de

from google.appengine.api import memcache, users

from yotsuba.lib import tori
from vps.config import UI

default_landing_page = '/admin'
login_url = users.create_login_url(default_landing_page)
logout_url = users.create_logout_url(default_landing_page)

def is_authenticated():
    return users.get_current_user() is not None

def is_authorized():
    user = users.get_current_user()
    return user and user.email() in tori.settings['admin']

class RESTSecureInterface(tori.RESTInterface):
    def mget(self, k):
        try: return memcache.get(k)
        except: return None
    
    def mset(self, k, v):
        try: memcache.set(k, v, 604800)
        except: pass
    
    def mset(self, k, v):
        try: memcache.set(k, v, 604800)
        except: pass
    
    def security_check(self):
        if not is_authenticated() or not is_authorized():
            self.respond_status(401)
            return False
        return True

class BasicSecureInterface(tori.BaseInterface):
    def mget(self, k):
        try: return memcache.get(k)
        except: return None
    
    def mset(self, k, v):
        try: memcache.set(k, v, 604800)
        except: pass
    
    def security_check(self):
        passed = True
        if not is_authenticated():
            self.redirect(login_url)
            passed = False
        elif not is_authorized():
            self.redirect('/e401')
            passed = False
        return passed
    
    def throw_e404_on_unknown_locale(self, lang):
        if not UI.has_translation(lang): self.respond_status(404)
    
    def render(self, lang, source, **context):
        global logout_url
        try:
            user = users.get_current_user()
            context['email'] = user.email()
        except:
            pass
        finally:
            context['logout_url'] = logout_url
            context['company_name'] = UI.translate(lang, 'company_name')
            context['lang'] = lang
        return super(BasicSecureInterface, self).render(source, **context)