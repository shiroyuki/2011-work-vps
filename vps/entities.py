from re import sub
from yotsuba.core import base
from google.appengine.ext import db
from vps.config import UI
from vps.exception import *
from google.appengine.runtime.apiproxy_errors import RequestTooLargeError
QUERY_LIMIT = 10000

def get_friendly_short_name(name):
    name = name.lower()
    name = sub("[\s/,\.\(\)\-\+\*\&\%\$\#\@\!\"\'\: ]+", "-", name)
    name = sub("^-+", "", name)
    name = sub("-+$", "", name)
    return name

class Entity(db.Model):
    def name(self, lang):
        return lang == "en" and self.en or self.th
    
    def id(self):
        return unicode(self.key())

class Category(Entity):
    en      = db.StringProperty(verbose_name="Name (English)", indexed=True)
    th      = db.StringProperty(verbose_name="Name (Thai)", indexed=True)
    ident   = db.StringProperty(verbose_name="Identifier", indexed=True)
    visible = db.BooleanProperty(default=False, indexed=True)
    color   = db.StringProperty(default="#ff6600", verbose_name="Color")
    
    @staticmethod
    def query(**params):
        q = Category.all()
        for k, v in params.iteritems():
            q.filter('%s =' % k, v)
        return q
    
    @staticmethod
    def find(**params):
        return Category.query(**params).fetch(QUERY_LIMIT)
    
    @staticmethod
    def get_by_ident(ident):
        c = Category.query(ident=ident).fetch(1)
        return c and c[0] or None
    
    @staticmethod
    def register(en, th):
        ident = get_friendly_short_name(en)
        if Category.find(ident=ident): raise EntityExisted, "This category is already existed."
        Category(en=en, th=th, ident=ident).put()
    
    @staticmethod
    def unregister(key):
        products = Product.all_in(key)
        for product in products: product.delete()
        category = Category.get(key)
        if category: category.delete()
        else: raise EntityNotExisted
    
    @staticmethod
    def init():
        default_categories = [
            'Bolt', 'Hook', 'Door guard', 'Latch', 'Hinge', 'Handle',
            'Furniture knob', 'Lockset',
            'Door stopper'
        ]
        default_categories.sort()
        query = Category.all()
        if query.count(): return
        for category in default_categories: Category.register(category, category)

class MiniBinary(Entity):
    origin = db.StringProperty(verbose_name="Origin", indexed=True)
    content_type = db.StringProperty()
    content = db.BlobProperty()
    
    @staticmethod
    def register(name, content_type, content):
        try:
            content = db.Blob(content)
            new_content = MiniBinary(origin=name, content_type=content_type, content=content)
            new_content.put()
        except RequestTooLargeError:
            return None
        return new_content

class Binary(Entity):
    name = db.StringProperty(verbose_name="Name", indexed=True)
    content = db.BlobProperty()
    content_type = db.StringProperty()
    stored_time = db.DateTimeProperty(auto_now_add=True)
    
    def readable_stored_time(self):
        return self.stored_time.strftime("%a, %d %b %Y %H:%M:%S %Z")
    
    @staticmethod
    def register(name, content_type, content):
        try:
            content = db.Blob(content)
            new_content = Binary(name=name, content_type=content_type, content=content)
            new_content.put()
        except RequestTooLargeError:
            return None
        return new_content
    
    @staticmethod
    def unregister(key):
        binary = Binary.get(key)
        if binary: binary.delete()
        else: raise EntityNotExisted

class Color(Entity):
    en    = db.StringProperty(verbose_name="Color (English)")
    th    = db.StringProperty(verbose_name="Color (Thai)")
    photo = db.ReferenceProperty(reference_class=Binary, verbose_name="Binary Object (Photo)")

class Material(Entity):
    en    = db.StringProperty(verbose_name="Color (English)")
    th    = db.StringProperty(verbose_name="Color (Thai)")

class Variation(Entity):
    en  = db.StringProperty(verbose_name="Description (English)")
    th  = db.StringProperty(verbose_name="Description (Thai)")
    size  = db.StringProperty(verbose_name="Size")
    materials = db.StringListProperty(verbose_name="Materials")
    
    def get_materials(self, lang=None):
        materials = []
        for mid in self.materials:
            m = Material.get(mid)
            if not m: continue
            if lang: materials.append(m.name(lang))
            else:    materials.append(m)
        return lang and ', '.join(materials) or materials

class Product(Entity):
    cat   = db.ReferenceProperty(reference_class=Category, verbose_name="Category", indexed=True)
    ident = db.StringProperty(verbose_name="Identifier", indexed=True)
    code  = db.StringProperty(verbose_name="Product Code", indexed=True)
    photos= db.StringListProperty(verbose_name="Photos")
    colors= db.StringListProperty(verbose_name="colors")
    variations= db.StringListProperty(verbose_name="Variations")
    
    def get_variations(self):
        variations = []
        for vid in self.variations:
            v = Variation.get(vid)
            if not v: continue
            variations.append(v)
        return variations
    
    def get_colors(self):
        colors = []
        for cid in self.colors:
            c = Color.get(cid)
            if not c: continue
            colors.append(c)
        return colors
    
    @staticmethod
    def query(**params):
        q = Product.all()
        for k, v in params.iteritems():
            q.filter('%s =' % k, v)
        q.order('ident')
        return q
    
    @staticmethod
    def find(**params):
        return Product.query(**params).fetch(QUERY_LIMIT)
    
    @staticmethod
    def get_by_ident(ident):
        p = Product.query(ident=ident).fetch(1)
        return p and p[0] or None
    
    @staticmethod
    def all_in(cat_key):
        cat = db.Key(cat_key)
        products = Product.find(cat = cat)
        return products
    
    @staticmethod
    def search_for(keyword, targeted_category=None):
        products = []
        query = keyword.lower()
        gql = Product.all()
        if targeted_category:
            cat = Category.get(targeted_category)
            if cat: gql.filter('cat =', cat)
        raw_products = gql.fetch(QUERY_LIMIT)
        for p in raw_products:
            score = 0
            if query in p.ident.lower() or query in p.code.lower():
                score += 1
            if not score:
                for v in p.get_variations():
                    if query in v.en.lower() or query in v.th.lower() or query in v.size.lower():
                        score += 1
                    if score: break
                    if not score:
                        for m in v.get_materials():
                            if query in m.en.lower() or query in m.th.lower():
                                score += 1
                            if score: break
                        # endfor
                    # endif
                # endfor
            if score: products.append(p)
        return products
    
    @staticmethod
    def register(cat, code):
        try:    cat = Category.get(cat)
        except: raise CategoryNotExisted
        ident = get_friendly_short_name(code)
        if Product.find(code=code) or Product.find(ident=ident): raise EntityExisted, "This product is already existed."
        Product(cat=cat, code=code, ident=ident).put()
    
    @staticmethod
    def unregister(key):
        product = Product.get(key)
        if product:
            for ekey in product.photos:
                try:    Binary.get(ekey).delete()
                except: pass
            for ekey in product.variations:
                try:    Variation.get(ekey).delete()
                except: pass
            product.delete()
        else:
            raise EntityNotExisted
