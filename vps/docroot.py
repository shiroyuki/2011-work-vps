# -*- encoding: utf-8 -*-

import re
from yotsuba.core   import base
from yotsuba.lib    import tori
from vps.admin      import Admin
from vps.base       import BasicSecureInterface
from vps.config     import UI
from vps.entities   import Category, Product, QUERY_LIMIT

class ReadOnlyInterface(BasicSecureInterface):
    def __init__(self, locale):
        self.__locale = locale
    
    def __render(self, page_title, page_code, source, **params):
        path_sectors = []
        
        for path_sector in tori.request.path_info.split("/"):
            if not path_sector: continue
            path_sectors.append(path_sector)
        
        path_info = '/'.join(path_sectors[1:])
        
        output = self.mget(tori.request.path_info)
        
        if not output:
            if "en" in path_sectors and path_sectors[0] == "en":
                path_info = '/th/%s' % path_info
            else:
                path_info = '/en/%s' % path_info
            
            output = self.render(
                self.__locale,
                source,
                page_title=page_title,
                page_code=page_code,
                path_info=path_info,
                **params
            )
            
            self.mset(tori.request.path_info, output)
        
        tori.response.headers['Cache-Control'] = 'max-age=604800, public' # one week
        
        return output
    
    @tori.webinterface
    def index(self):
        return self.__render(UI.translate(self.__locale, 'page_title_index'), 'index', '%s-index.html' % self.__locale)
    
    @tori.webinterface
    def contact(self):
        return self.__render(UI.translate(self.__locale, 'page_title_contact'), 'contact', '%s-contact.html' % self.__locale)
    
    @tori.webinterface
    def products(self, cat_ident=None, prod_ident=None, q=None):
        key   = '%s/%s/%s/%s' % (self.__locale, cat_ident, prod_ident, q)
        cache = self.mget(key)
        
        if not cache:
            status     = 200
            categories = Category.all().fetch(QUERY_LIMIT)
            category = None
            products = None
            product  = None
            query    = q or ''
            
            if cat_ident:
                cat_ckey = 'categories/%s' % cat_ident
                category = Category.get_by_ident(cat_ident)
            
            if cat_ident and prod_ident:
                product = Product.get_by_ident(prod_ident)
                if not product:
                    status = 404
                elif product.cat.id() != category.id():
                    status = 410
            elif cat_ident and not prod_ident:
                products = Product.all_in(category.id())
            elif query:
                products = Product.search_for(query)
        
            cache = {
                'status': status,
                'output': self.__render(
                    UI.translate(self.__locale, 'page_title_products'), 'products', 'products.html',
                    enable_extra=tori.settings['enable_extra'],
                    status=status, cat_ident=cat_ident, prod_ident=prod_ident, query=query,
                    categories=categories, category=category, products=products, product=product
                )
            }
            
            self.mset(key, cache)
        
        if cache['status'] != 200:
            self.respond_status(cache['status'], break_now=False)
        
        return cache['output']

class DocumentRoot(BasicSecureInterface):
    en = ReadOnlyInterface('en')
    th = ReadOnlyInterface('th')
    admin = Admin()
    
    @tori.webinterface
    def index(self, show_homepage=None):
        ''' WARNING: show_homepage is not a permanent switch. '''
        output = self.mget('homepage')
        if not output:
            output = self.render('en', 'index.html')
            self.mset('homepage', output)
        return output
    
    @tori.webinterface
    def e401(self):
        tori.response.status_code = 401
        return self.render('en', 'e401.html')