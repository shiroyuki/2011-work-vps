class NoTranslation(Exception): pass
class DataTooLarge(Exception): pass
class EntityExisted(Exception): pass
class EntityNotExisted(Exception): pass
class CategoryNotExisted(Exception): pass