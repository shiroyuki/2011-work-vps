var KEYCODE_L = 37;
var KEYCODE_R = 39;
var KEYCODE_U = 38;
var KEYCODE_D = 40;
var KEYCODE_E = 27;
var page_number = 0;
var number_of_products_per_page = 12;
var number_of_pages = 1;
var products = null;
function select_image(e) {
    e.preventDefault();
    var id = String($(this).attr('href')).replace(/^.+\/s\//, '');
    pt.children().each(function(index) {
        var lid = String($(this).attr('src')).replace(/^.+\/s\//, '');
        if (id === lid) {
            $(this).click();
            return false;
        }
        return true;
    });
    pa.show();
}
function select_page(e) {
    e.preventDefault();
    flip_to_page(parseInt($(this).attr('data-value')));
}

function flip_to_page(page_number) {
    var beginning   = page_number * number_of_products_per_page;
    var end         = beginning + number_of_products_per_page;
    var buttons     = $('#prod-navi');
    buttons.children().removeClass('selected');
    buttons.children('[data-value=' + page_number + ']').addClass('selected');
    products.each(function (i) {
        if (beginning <= i && i < end) {
            products.eq(i).show();
        } else {
            products.eq(i).hide();
        }
    });
}

$(document).ready(function() {
    if ($("#products").length == 0) {
        p = $("#product");
        p1 = p.find(".first-photo");
        pn = p.find(".photos");
        pa = p.find(".all-photos");
        pd = pa.find('.display');
        pt = pa.find('.thumbnails');
        
        p.addClass("jse");
        pa.hide().removeClass('hidden');
        p1.find("a").click(select_image);
        pn.find("a").click(select_image);
        pa.find(".thumbnails img").click(function(e) {
            var code = $(this).clone();
            pd.html(code);
            pt.find("img.selected").removeClass('selected');
            $(this).addClass('selected');
        });
        $(window).keyup(function(e) {
            kc = e.keyCode;
            if (kc == KEYCODE_E) {
                if (pa.is(':visible')) {
                    pa.hide();
                } else {
                    pa.show();
                    if (pt.find('.selected').length === 0) {
                        pt.children().eq(0).addClass('selected');
                    }
                }
            } else if (kc == KEYCODE_L || kc == KEYCODE_R) {
                var see_next = kc === KEYCODE_R;
                var curr = pt.find(".selected");
                var nb = see_next?curr.next():curr.prev();
                if (nb.length == 0) {
                    return;
                }
                curr.removeClass('selected');
                nb.addClass('selected');
                pd.html(nb.clone());
            }
        });
    } else {
        products = $("#products li");
        number_of_pages = Math.ceil( products.length / parseFloat(number_of_products_per_page) );
        if (number_of_pages > 1) {
            $("#products").before([
                '<div id="prod-navi">','</div>'
            ].join(' '));
            for (var i = 0; i < number_of_pages; ++i) {
                $('#prod-navi').append('<a data-value="' + i + '">' + (i + 1) + '</a>');
            }
            flip_to_page(0);
        }
        $("#prod-navi a").click(select_page);
    }
});