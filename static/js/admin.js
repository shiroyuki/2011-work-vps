var panels;
var dialog;
var product_key;

var available_colors = [
    '#ff6600',
    '#00cc66',
    '#6600cc',
    '#66cc00',
    '#0066cc',
    '#cc0066',
    '#996633',
    '#339966',
    '#663399',
    '#669933',
    '#336699',
    '#993366'
]

ui = {
    request_in_progress: {
        en: 'Processing your request. Please do not close this window or tab.',
        th: 'กำลังประมวลผล กรุณาอย่าปิดหน้าต่าง หรือแทบนี้'
    },
    need_to_complete_form: {
        en: 'Please complete the form.',
        th: 'กรุณาให้ข้อมูลทุกอย่าง'
    },
    e500: {
        en: 'Service temporarily unavailable',
        th: 'ระบบไม่สามารถให้บริการชั่วคราว'
    },
    error_on_category_update: {
        en: 'Category with this name already existed. Try again.',
        th: 'กรุณาพยายามใช้ชื่ออื่น'
    },
    error_on_product_update: {
        en: 'Product with this article number or similar English description already existed. Try again.',
        th: 'กรุณาพยายามใช้รหัสสินค้าอื่น หรือคำบรรยาย (เฉพาะภาษาอังกฤษเท่านั้น) อื่น'
    },
    confirm_on_entity_deletion: {
        en: 'This operation is NOT reversible. Are you sure?',
        th: 'เมื่อคุณตกลงที่จะลบ คุณจะไม่สามารถยกเลิกได้ คุณยังแน่ใจที่จะลบข้อมูลนี้หรือไม่'
    },
    confirm_on_category_deletion: {
        en: 'You are about to delete all products in this category. This operation is not reversible. Are you sure that you want to proceed?',
        th: 'คุณกำลังจะลบข้อมูลของสินค้าทุกอย่างในหมวดนี้ เมื่อคุณลบแล้ว จะไม่สามารถกู้ข้อมูลกลับได้ กด Cancel เพื่อยกเลิก หรือ Ok เพื่อลบข้อมูลของสินค้าทุกอย่างในประเภทนี้'
    },
    confirm_on_product_deletion: {
        en: 'You are about to delete this product from the database. This operation is not reversible. Are you sure that you want to proceed?',
        th: 'คุณกำลังจะลบข้อมูลของสินค้าอันนี้จากฐานข้อมูล เมื่อคุณลบแล้ว จะไม่สามารถกู้ข้อมูลกลับได้ กด Cancel เพื่อยกเลิก หรือ Ok เพื่อลบข้อมูลของสินค้าทุกอย่างในประเภทนี้'
    }
}

function load_colors() {
    shiroyuki.ajax.get('/admin/cl', {}, function(xml) {
        var r = $(xml);
        var selections = $('.available-colors');
        selections.empty();
        $('#colors li').addClass("hidden")
        r.find('entity[kind=Color]').each(function(j) {
            var k = $(this).attr('key');
            var n = $(this).find('[name=' + lang + ']').text();
            var p  = $(this).find('[name=photo]').text().match(/\[([^\]]+)\]/)[1];
            if ($("#" + k).length == 0) {
                $('#colors').append('<li id="' + k + '"></li>');
            } else {
                $('#' + k).removeClass('hidden');
            }
            $('#' + k).html([
                '<a class="show"><img src="/admin/s/', p, '"/> ', n,
                '</a><a class="delete">&times;</a></li>'
            ].join(''));
            selections.append([
                '<li id="',k,'"><span class="image"><img src="/admin/s/',p,'"/></span><span class="name">',n,'</span></li>'
            ].join(''));
            //console.log('Color: ' + k);
        });
        $('#colors li.hidden').remove();
    });
}

function load_materials() {
    shiroyuki.ajax.get('/admin/m', {}, function(xml) {
        var r = $(xml);
        $('.available-materials').empty();
        $('#materials li').addClass("hidden")
        r.find('entity[kind=Material]').each(function(j) {
            var k = $(this).attr('key');
            var n = $(this).find('[name=' + lang + ']').text();
            if ($("#" + k).length == 0) {
                $('#materials').append('<li id="' + k + '"></li>');
            } else {
                $('#' + k).removeClass('hidden');
            }
            $('#' + k).html([
                '<a class="show">', n,
                '</a><a class="delete">&times;</a></li>'
            ].join(''));
            $('.available-materials').append([
                '<li id="',k,'">',n,'</li>'
            ].join(''));
        });
        $('#colors li.hidden').remove();
    });
}

function load_categories() {
    shiroyuki.ajax.get('/admin/c', {}, function(xml) {
        var r = $(xml);
        //$('#categories').empty();
        $('.available-categories').empty();
        $('#categories li').addClass("hidden")
        r.find('entity[kind=Category]').each(function(j) {
            var k = $(this).attr('key');
            var i = $(this).find('[name=ident]').text();
            var n = $(this).find('[name=' + lang + ']').text();
            var c = $(this).find('[name=color]').text();
            if ($("#" + k).length == 0) {
                $('#categories').append('<li id="' + k + '"></li>');
            } else {
                $('#' + k).removeClass('hidden');
            }
            $('#' + k).html('<a class="show"><span style="color:' + c + ' !important;">●</span> ' + n
                + '</a><a class="delete">&times;</a></li>'
            );
            $('.available-categories').append([
                '<option value="',k,'">',n,'</option>'
            ].join(''));
        });
        $('#categories li.hidden').remove();
        tell_user_the_current_category();
    });
}

function load_products() {
    var options = {}
    var selected_cats = $("ul#categories li.selected");
    if (selected_cats.length > 0) {
        options.categorized_in = selected_cats.attr('id');
        $('.column.categories .commands .edit_category').show();
    } else {
        $('.column.categories .commands .edit_category').hide();
    }
    shiroyuki.ajax.get('/admin/p', options, function(xml) {
        var r = $(xml);
        var output = []
        var products = r.find('entity[kind=Product]');
        products.each(function(j) {
            var k = $(this).attr('key');
            var i = $(this).find('[name=ident]').text();
            var c = $(this).find('[name=code]').text();
            var p = $(this).find('[name=photos]').length>0?$(this).find('[name=photos]:first').text():null;
            output.push([
                '<li id="', k, '"><a class="show"><!--',
                (p?'<img src="/admin/s/'+p+'"/>':'<span class="img-rpm">N/A</span>'),
                '--><span>', c, '</span></a>',
                '<a class="delete">&times;</a>',
                '</li>'
            ].join(''));
        });
        if (products.length == 0) {
            $('#products').html('<li class="empty">Empty</li>');
        } else {
            $('#products').html(output.join(''));
        }
    });
}

function load_product_images() {
    var t = $('.product-prompt.edit .photos');
    var output = [];
    if (!product_key) {
        t.empty()
        return;
    } else {
        //console.log("product key: " + product_key);
    }
    shiroyuki.ajax.get('/admin/p/' + product_key, {}, function(xml) {
        var t = $('.product-prompt.edit .photos');
        var r = $(xml);
        var k = r.find("entity").attr("key");
        r.find("[name=photos]").each(function(i) {
            var j = $(this).text();
            //console.log("binary key: " + j);
            output.push(['<li data-key="', k, '" data-bin="', j,'"><img src="/admin/s/', j, '"/></li>'].join(''));
        });
        t.html(output.join(''));
    });
}

function load_product_variations() {
    var t = $('#variations');
    var output = [];
    if (!product_key) {
        t.empty()
        return;
    } else {
        //console.log("product key: " + product_key);
    }
    t.append("!!");
    shiroyuki.ajax.get('/admin/v/' + product_key, {}, function(xml) {
        var r = $(xml);
        r.find("[kind=Variation]").each(function(i) {
            var k = $(this).attr("key");
            var en = $(this).find("[name=en]").text();
            var th = $(this).find("[name=th]").text();
            var s = $(this).find("[name=size]").text();
            var mtrs = [];
            $(this).find("[name=materials]").each(function(j) {
                var m = r.find('[key=' + $(this).text() + '] [name=' + lang + ']').text();
                mtrs.push(m);
            });
            var m = mtrs.join(', ');
            
            //console.log("variation key: " + k + ' (' + m + ')');
            output.push(['<tr id="', k, '" data-parent="', product_key, '">'].join(''));
            output.push([
                '<td class="commands"><a class="delete">&times;</a></td>',
                '<td class="en">', en, '</td>',
                '<td class="th">', th, '</td>',
                '<td class="size">', s, '</td>',
                '<td class="materials">', m, '</td>'
            ].join(''));
            output.push(['</tr>'].join(''));
        });
        if (output.length > 0) {
            output = '<tr><th></th><th>Description (English)</th><th>รายละเอียด (ภาษาไทย)</th><th>'
                + (lang=="en"?'Size':'ขนาด') + '</th><th>'
                + (lang=="en"?'Materials':'วัสดุ') + '</th></tr>'
                + output.join('')
        }
        t.html(output);
    });
}

function cb_delete_category(response) {
    if ($("#categories li.selected").length == 0)
        $('.column.categories .commands .edit_category').hide();
    load_categories();
    load_products();
}

function cb_delete_product(response) {
    load_products();
}

function cb_delete_color(response) {
    load_colors();
}

function cb_delete_material(response) {
    load_materials();
}

function cb_delete_variation(response) {
    load_product_variations();
}

function generic_error(e) {
    shiroyuki.notifier.post(ui.e500[lang], 'error', 5);
}

function product_update_error(e) {
    if (e.status == 403) {
        shiroyuki.notifier.post(ui.error_on_product_update[lang], 'error', 5);
    } else {
        shiroyuki.notifier.post(ui.e500[lang], 'error', 5);
    }
}

function show_dialog(target) {
    dialog.children(target).show();
    dialog.removeClass('hidden');
    dialog.show();
}

function hide_dialog() {
    dialog.children("div").hide();
    dialog.removeClass('hidden');
    dialog.hide();
}

function bind_ajax_form(target, on_success, on_failure) {
    $(target).addClass('ajax-form');
    $(target).live("submit", function(e) {
        e.preventDefault();
        var prevent_ajax_call = false;
        var form = $(this);
        var params = {};
        form.find("select,input").each(function(i) {
            params[$(this).attr('name')] = $(this).val();
            //console.log($(this).attr('name') + " > " + $(this).val());
            if (
                $.trim(params[$(this).attr('name')]).length == 0
                && !$(this).hasClass('not-required')
            )
                prevent_ajax_call = true;
        });
        if (prevent_ajax_call) {
            shiroyuki.notifier.post(ui.need_to_complete_form[lang], 'error', 5);
            return;
        }
        shiroyuki.notifier.post(ui.request_in_progress[lang]);
        shiroyuki.ajax.send(
            form.parent().hasClass('new')?'post':'put',
            form.attr('action'), params, on_success, on_failure
        );
    });
}

function create_color_picker() {
    var color_picker = $('.category-prompt form .color');
    color_picker.empty();
    for (var i in available_colors) {
        color_picker.append(
            '<a style="background-color:__CODE__;" data-color="__CODE__" class="__SELECTED__"></a>'
            .replace(/__CODE__/g, available_colors[i])
            .replace(/__SELECTED__/g, i==0?"selected":"")
        );
        color_picker.children('a:first').click();
    }
}

function tell_user_the_current_category() {
    var elem = $('ul#categories li.selected');
    if (elem.length > 0) {
        $('.column.products h2 > span').html((lang=="en"?"categorized in ":"ในหมวด") + elem.find('a.show').html());
    } else {
        $('.column.products h2 > span').empty();
    }
}

$(document).ready(function(e) {
    panels = $(".panels");
    dialog = $("#dialog");
    
    /* Form: register/update category */
    bind_ajax_form(".material-prompt form:first", function(r) {
            shiroyuki.notifier.empty();
            hide_dialog();
            load_materials();
        }, generic_error
    );
    
    /* Form: register/update category */
    bind_ajax_form(".category-prompt form:first", function(r) {
            shiroyuki.notifier.empty();
            hide_dialog();
            load_categories();
        }, function(e) {
            if (e.status == 403) {
                shiroyuki.notifier.post(ui.error_on_category_update[lang], 'error', 5);
            } else {
                shiroyuki.notifier.post(ui.e500[lang], 'error', 5);
            }
        }
    );
    
    /* Form: register/update product variations */
    bind_ajax_form(".product-prompt form.variation",
        function(r) {
            shiroyuki.notifier.empty();
            load_product_variations();
            var f = $(".product-prompt form.variation");
            f.find("input").val('');
            f.find("li").removeClass('selected');
        }, product_update_error
    );
    
    /* Form: register/update product colors */
    bind_ajax_form(".product-prompt form.colors",
        function(r) {
            shiroyuki.notifier.empty();
        }, product_update_error
    );
    
    /* Form: register/update product */
    bind_ajax_form(".product-prompt form:first", function(r) {
            shiroyuki.notifier.empty();
            hide_dialog();
            load_products();
        }, product_update_error
    );
    
    /* Handle photo uploader */
    $(".color-prompt form, .product-prompt .uploader").submit(function(e) {
        var prevent_ajax_call = false;
        var form = $(this);
        var params = {};
        form.find("select,input").each(function(i) {
            params[$(this).attr('name')] = $(this).val();
            if ($.trim(params[$(this).attr('name')]).length == 0 && !$(this).hasClass('not-required')) prevent_ajax_call = true;
        });
        if (prevent_ajax_call) {
            shiroyuki.notifier.post(ui.need_to_complete_form[lang], 'error', 5);
            return false;
        }
        shiroyuki.notifier.post(ui.request_in_progress[lang]);
        return true;
    });
    
    /* Entity Deletion: Category */
    $("ul#categories a.delete").live("click", function(e) {
        e.preventDefault();
        if(!confirm(ui.confirm_on_category_deletion[lang])) return;
        $(this).parent().fadeOut();
        shiroyuki.ajax.del(
            '/admin/c/' + $(this).parent().attr('id'),
            cb_delete_category, generic_error
        );
    });
    
    /* Filter the product list */
    $('ul#categories li').live("click", function(e) {
        e.preventDefault();
        $(this).parent().children().not(this).removeClass("selected");
        $(this).toggleClass("selected");
        tell_user_the_current_category();
        load_products();
    });
    
    /* Entity View: Category */
    $(".column.categories .edit_category").live("click", function(e) {
        e.preventDefault();
        shiroyuki.ajax.get(
            '/admin/c/' + $('#categories .selected').attr('id'), {},
            function(xml) {
                var l = ".category-prompt";
                var t = dialog.find(l);
                var r = $(xml).find('entity[kind=Category]');
                var k = r.attr('key');
                var i = r.find('[name=ident]').text();
                var c = r.find('[name=color]').text();
                var n_en = r.find('[name=en]').text();
                var n_th = r.find('[name=th]').text();
                create_color_picker();
                t.find('form').attr('action', t.find('form').attr('data-action') + k);
                t.find("[data-color=" + c + "]").click();
                t.find("[name=en]").val(n_en);
                t.find("[name=th]").val(n_th);
                hide_dialog();
                $('.category-prompt').removeClass('new');
                $('.category-prompt').addClass('edit');
                $('.category-prompt .edit').show();
                $('.category-prompt .new').hide();
                show_dialog(l);
            }
        );
    });
    
    /* Entity Deletion: Product */
    $("ul#products a.delete").live("click", function(e) {
        e.preventDefault();
        if(!confirm(ui.confirm_on_product_deletion[lang])) return;
        $(this).parent().fadeOut();
        shiroyuki.ajax.del(
            '/admin/p/' + $(this).parent().attr('id'),
            cb_delete_product, generic_error
        );
    });
    
    /* Entity Deletion: Color */
    $("ul#colors a.delete").live("click", function(e) {
        e.preventDefault();
        if(!confirm(ui.confirm_on_entity_deletion[lang])) return;
        $(this).parent().fadeOut();
        shiroyuki.ajax.del(
            '/admin/cl/' + $(this).parent().attr('id'),
            cb_delete_color, generic_error
        );
    });
    
    /* Entity Deletion: Material */
    $("ul#materials a.delete").live("click", function(e) {
        e.preventDefault();
        if(!confirm(ui.confirm_on_entity_deletion[lang])) return;
        $(this).parent().fadeOut();
        shiroyuki.ajax.del(
            '/admin/m/' + $(this).parent().attr('id'),
            cb_delete_material, generic_error
        );
    });
    
    /* Entity Deletion: Variation */
    $("table#variations a.delete").live("click", function(e) {
        e.preventDefault();
        if(!confirm(ui.confirm_on_entity_deletion[lang])) return;
        $(this).parent().fadeOut();
        shiroyuki.ajax.del(
            '/admin/v/' + product_key + '/' + $(this).parent().parent().attr('id'),
            cb_delete_variation, generic_error
        );
    });
    
    /* Entity Deletion: Binary */
    $('.product-prompt .photos li').live('click', function(e) {
        $(this).fadeOut();
        var key = $(this).attr('data-key');
        var binary_id = $(this).attr('data-bin');
        shiroyuki.ajax.del('/admin/s/product/' + key + '/' + binary_id,
            function(r) {
                load_product_images();
            }, function(e) {
                shiroyuki.notifier.post(ui.e500[lang], 'error', 5);
            }
        );
    });
    
    /* Entity View: Product */
    $("ul#products a.show").live("click", function(e) {
        e.preventDefault();
        product_key = $(this).parent().attr('id');
        shiroyuki.ajax.get(
            '/admin/p/' + product_key, {},
            function(xml) {
                var l = ".product-prompt";
                var t = dialog.find(l);
                var r = $(xml).find('entity[kind=Product]');
                var k = r.attr('key');
                var i = r.find('[name=ident]').text();
                var c = r.find('[name=code]').text();
                var cat  = r.find('[name=cat]').text().match(/\[([^\]]+)\]/)[1];
                load_product_images();
                load_product_variations();
                t.find('form:first, form.colors').attr('action', t.find('form:first').attr('data-action') + k);
                t.find('.only-for-update').show();
                t.find('.only-for-update form.uploader [name=key]').val(k);
                t.find("[name=code]").val(c);
                t.find("[name=cat]").val(cat);
                hide_dialog();
                $('.product-prompt').removeClass('new');
                $('.product-prompt').addClass('edit');
                $('.product-prompt .edit').show();
                $('.product-prompt .new').hide();
                $('.product-prompt .tabs a').not(':first').removeClass('selected');
                $('.product-prompt .tabs a:first').addClass('selected');
                $('.product-prompt .tab-body').not(':first').hide();
                $('.product-prompt .tab-body:first').show();
                show_dialog(l);
                
                $('.product-prompt form.variation').parent().addClass('new');
                $('.product-prompt form.variation').attr('action', $('.product-prompt form.variation').attr('data-action') + product_key);
                var ac = $('.product-prompt form.colors .available-colors');
                ac.find('li').removeClass('selected');
                r.find('[name=colors]').each(function(i) {
                    var cid = $(this).text();
                    ac.find('#' + cid).addClass('selected');
                });
            }
        );
    });
    
    /* Entity View: Color */
    $("ul#colors a.show").live("click", function(e) {
        e.preventDefault();
        key = $(this).parent().attr('id');
        shiroyuki.ajax.get(
            '/admin/cl/' + key, {},
            function(xml) {
                var l = ".color-prompt";
                var t = dialog.find(l);
                var r = $(xml).find('entity[kind=Color]');
                var k = r.attr('key');
                var e = r.find('[name=en]').text();
                var h = r.find('[name=th]').text();
                var p = r.find('[name=photo]').text().match(/\[([^\]]+)\]/)[1];
                t.find("[name=en]").val(e);
                t.find("[name=th]").val(h);
                t.find("[name=uploaded_content]").val(''); // reset the file input.
                hide_dialog();
                $('.color-prompt').removeClass('new');
                $('.color-prompt').addClass('edit');
                $('.color-prompt .edit').show();
                $('.color-prompt .new').hide();
                $('.color-prompt form').append('<input name="key" type="hidden" value="' + k + '"/>');
                $('.color-prompt .sample').html('<img src="/admin/s/' + p + '"/>');
                $('.color-prompt form input[type=file]').addClass('not-required');
                show_dialog(l);
            }
        );
    });
    
    /* Entity View: Materials */
    $("ul#materials a.show").live("click", function(e) {
        e.preventDefault();
        key = $(this).parent().attr('id');
        shiroyuki.ajax.get(
            '/admin/m/' + key, {},
            function(xml) {
                var l = ".material-prompt";
                var t = dialog.find(l);
                var r = $(xml).find('entity[kind=Material]');
                var k = r.attr('key');
                var e = r.find('[name=en]').text();
                var h = r.find('[name=th]').text();
                t.find('form:first').attr('action', t.find('form:first').attr('data-action') + k);
                t.find("[name=en]").val(e);
                t.find("[name=th]").val(h);
                hide_dialog();
                t.removeClass('new');
                t.addClass('edit');
                t.find('.edit').show();
                t.find('.new').hide();
                show_dialog(l);
            }
        );
    });
    
    /* Deactivator: extra panel */
    $("#dialog button[type=reset]").live("click", function(e) {
        e.preventDefault();
        hide_dialog();
    });
    
    /* Material selector */
    $(".product-prompt .available-materials li, .product-prompt .available-colors li").live("click", function(e) {
        e.preventDefault();
        var items = [];
        $(this).toggleClass('selected');
        $(this).parent().children("li.selected").each(function(i) {
            items.push($(this).attr('id'));
        });
        $(this).parent().next().val(items.join(','));
    });
    
    /* Color picker */
    $('.category-prompt form .color a').live("click", function(e) {
        e.preventDefault();
        $(this).parent().children().removeClass("selected");
        $(this).addClass("selected");
        $(this).parent().children("input").remove();
        $(this).parent().append('<input type="hidden" name="color" value="' + $(this).attr('data-color') + '"/>');
    });
    
    $('h1 a').click(function(e) {
        if ($(this).hasClass('selected')) return;
        $('h1 a').removeClass('selected');
        $(this).addClass('selected');
        $('#content > .layout').hide();
        $('#content > .layout.' + $(this).attr('data-id')).show();
    });
    
    $('.product-prompt .tabs a').click(function(e) {
        if ($(this).hasClass('selected')) return;
        $('.product-prompt .tabs a').removeClass('selected');
        $(this).addClass('selected');
        $('.product-prompt .tab-body').hide();
        $('.product-prompt .tab-body[data-id=' + $(this).attr('data-id') + ']').show();
    });
    
    /* Activator: Material Registration */
    $(".add_material").live("click", function(e) {
        e.preventDefault();
        $('.material-prompt').find('input').val('');
        create_color_picker();
        hide_dialog();
        $('.material-prompt form').attr('action', $('.material-prompt form').attr('data-action'));
        $('.material-prompt').removeClass('edit');
        $('.material-prompt').addClass('new');
        $('.material-prompt .edit').hide();
        $('.material-prompt .new').show();
        show_dialog('.material-prompt');
    });
    
    /* Activator: Color Registration */
    $(".add_color").live("click", function(e) {
        e.preventDefault();
        $('.color-prompt').find('input').val('');
        $('.color-prompt').find('input[name=key]').remove();
        $('.color-prompt').find('.sample').empty();
        create_color_picker();
        hide_dialog();
        $('.color-prompt form').attr('action', $('.color-prompt form').attr('data-action'));
        $('.color-prompt form input[type=file]').removeClass('not-required');
        $('.color-prompt').removeClass('edit');
        $('.color-prompt').addClass('new');
        $('.color-prompt .edit').hide();
        $('.color-prompt .new').show();
        show_dialog('.color-prompt');
    });
    
    /* Activator: Category Registration */
    $(".add_category").live("click", function(e) {
        e.preventDefault();
        $('.category-prompt').find('input, select').val('');
        create_color_picker();
        hide_dialog();
        $('.category-prompt form:first').attr('action', $('.category-prompt form').attr('data-action'));
        $('.category-prompt').removeClass('edit');
        $('.category-prompt').addClass('new');
        $('.category-prompt .edit').hide();
        $('.category-prompt .new').show();
        show_dialog('.category-prompt');
    });
    
    /* Activator: Product Registration */
    $(".add_product").live("click", function(e) {
        e.preventDefault();
        hide_dialog();
        product_key = null;
        load_product_images();
        $('.product-prompt form:first').attr('action', $('.product-prompt form').attr('data-action'));
        $('.product-prompt .only-for-update').hide();
        $('.product-prompt').removeClass('edit');
        $('.product-prompt').addClass('new');
        $('.product-prompt .edit').hide();
        $('.product-prompt .new').show();
        $('.product-prompt .tab-body').hide();
        $('.product-prompt').find('input, select').val('');
        show_dialog('.product-prompt');
    });
    
    /* Bootstab */
    shiroyuki.notifier.install();
    shiroyuki.no_highlight.__init__("h1, h2, ul, .commands");
    $(".mc").hide().removeClass('hidden');
    hide_dialog();
    load_colors();
    load_categories();
    load_materials();
    load_products();
});
