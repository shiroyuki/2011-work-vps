from pickle             import dump
from yotsuba.core       import fs
from yotsuba.core.net   import Http as h
from yotsuba.lib.kotoba import Kotoba

url_base = 'http://localhost:8080'
url_backup_interface = '%s/admin/archive' % url_base

response = h.get(url_backup_interface)
content  = response.content()
xml      = Kotoba(content)

fs.write('backup.xml', content)

print 'Backup Archive Size: %.3f KB' % (len(content)/1024.0)

for binary in xml.get('entity[kind=Binary]'):
    print 'Caching: %(key)s (%(kind)s)' % binary.attrs