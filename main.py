from yotsuba.lib import tori
from vps.docroot import DocumentRoot

if __name__ == "__main__":
    tori.setup('config.xml')
    application = tori.ServerInterface.auto(DocumentRoot())